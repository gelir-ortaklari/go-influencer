<?php


namespace App\Util;


use App\Util\DeepLinker\Morhipo;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class DeepLinker
{
    private $content;
    private $links=[];
    private $ids=[];
    public $golink = 'http://tr.rdrtr.com/aff_c?offer_id=%d&aff_id=%d&aff_sub=%s&url=%s';
    public $universallink = 'https://y7l8.adj.st/universal/%s%s&adj_deeplink=%s&adj_redirect=%s';
    private $jsrLink = 'https://app.adjust.com/jsr?url=%s';
    private $link;
    private $title;
    private $weblink;
    private $applink;

    public $mobileUrl ='';
    public $adj =[];

    private $cuttyApiKey = '';

    /**
     * Morhipo constructor.
     */
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->cuttyApiKey = $parameterBag->get('CUTTY');
    }
    abstract function convert();

    public function create($content,$title,$affId,$adjustParams,$utmParams)
    {
        $this->title =  $title;
        $this->content = $content;
        $this->findAllLink();
        $this->convert();

        $utms = $this->paramToUrl($utmParams);

        $adjRedirectEncoded = urlencode($this->getWeblink().($utms!="" ? '&' : '').$utms);
        $adjDeepLinkEncoded = urlencode($this->getApplink().($utms!="" ? '&' : '').$utms);
        $golinkEncoded = urlencode(sprintf($this->golink,$affId,$this->title,$adjRedirectEncoded));
        $uniFirst = str_replace($this->mobileUrl,"",$this->getApplink());
        $adjustParams = $this->paramToUrl(array_merge($adjustParams,$this->adj,['adj_creative'=>$this->getTitle()]));
        $glue = $adjustParams!="" ? "&" : "";
        $uniAdJsutParams = $glue.$adjustParams.$glue;

        $universalLink = sprintf($this->universallink,$uniFirst,$uniAdJsutParams,$adjDeepLinkEncoded,$golinkEncoded);
        $this->link = sprintf($this->jsrLink,urlencode($universalLink));
        $alias = sprintf('%s-%s-%d', $utmParams['utm_campaign'], $affId, random_int(1, 1000));

        return json_decode(file_get_contents(sprintf('https://cutt.ly/api/api.php?key=%s&short=%s&name=%s', $this->cuttyApiKey, urlencode($this->getLink()), $alias)));
    }

    private function paramToUrl($array)
    {
        $return=[];
        foreach ($array as $index => $value)
        {
            $return[]= $index.'='.$value;
        }
        return implode($return,'&');
    }
    private function findAllLink()
    {
        if($num_found = preg_match_all('~[a-z]+://\S+~', $this->content, $out))
        {
            $this->links = $out[0];
        }
    }
    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    /**
     * @return string
     */
    public function getWeblink()
    {
        return $this->weblink;
    }

    /**
     * @param string $weblink
     */
    public function setWeblink($weblink)
    {
        $this->weblink = $weblink;
    }

    /**
     * @return string
     */
    public function getApplink()
    {
        return $this->applink;
    }

    /**
     * @param string $applink
     */
    public function setApplink($applink)
    {
        $this->applink = $applink;
    }

}