<?php


namespace App\Util\DeepLinker;


use App\Util\DeepLinker;

class Boyner extends DeepLinker
{
    const ID_REGEX = '/([a-z]+)-(\d+)/';
    const ID_SEP = ',';
    const PREFIX = '';
    const SULFIX ='';
    const WEB_URL = 'https://www.boyner.com.tr/';
    public $mobileUrl = 'boyner://';
    const PATH = 'search?q=';
    public $adj = [
        'adj_t'=>'ej4jva_xpo0t2',
        'adj_campaign' => 'influencer_6568'
    ];

    public $golink = 'http://tr.rdrtr.com/aff_c?offer_id=6568&aff_id=%d&aff_sub=%s&url=%s';
    public $universallink = 'https://w49t.adj.st/%s%s&adj_deeplink=%s&adj_redirect=%s';

    /**
     * Boyner constructor.
     */
    public function convert()
    {
        $this->setTitle(str_replace(['\''],["%27"],urlencode($this->getTitle())));
        $ids=[];
        foreach ($this->getLinks() as $link)
        {
            preg_match(self::ID_REGEX, $link, $matches);
            $ids[] =$matches[2];
        }
        $searhString = self::PREFIX.implode(self::ID_SEP,$ids).self::SULFIX.'&title='.$this->getTitle();
        $this->setApplink($this->mobileUrl.self::PATH.$searhString);
        $this->setWeblink(self::WEB_URL.self::PATH.$searhString);
    }




}