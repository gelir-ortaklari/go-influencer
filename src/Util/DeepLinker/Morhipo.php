<?php


namespace App\Util\DeepLinker;


use App\Util\DeepLinker;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Morhipo extends DeepLinker
{
    const ID_REGEX = '/(?!\/)([0-9]*)(?=\/)/';
    const ID_SEP = '%2C';
    const PREFIX = '%5B';
    const SULFIX ='%5D';
    const WEB_URL = 'https://www.morhipo.com/';
    public $mobileUrl = 'morhipo://';
    const PATH = 'kampanya/arama?q=';
    public $adj = [
        'adj_t'=>'t66ajrq_c8st647',
        'adj_campaign' => 'influencer'
    ];

    public $golink = 'http://tr.rdrtr.com/aff_c?offer_id=2124&aff_id=%d&aff_sub=%s&url=%s';
    public $universallink = 'https://y7l8.adj.st/universal/%s%s&adj_deeplink=%s&adj_redirect=%s';


    /**
     * Morhipo constructor.
     */
    public function convert()
    {
        $this->setTitle(str_replace(['+','\''],["%20","%27"],urlencode($this->getTitle())));
        $ids=[];
        foreach ($this->getLinks() as $link)
        {
            preg_match(self::ID_REGEX, $link, $matches);
            $ids[] =$matches[0];
        }
        $searhString = self::PREFIX.implode(self::ID_SEP,$ids).self::SULFIX.'&qcat=campaign&searchtitle='.$this->getTitle();
        $this->setApplink($this->mobileUrl.self::PATH.$searhString);
        $this->setWeblink(self::WEB_URL.self::PATH.$searhString);
    }




}