<?php

namespace App\Controller;


use App\Util\DeepLinker\Boyner;
use App\Util\DeepLinker\Morhipo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DeepLinkerController extends AbstractController
{
    /**
     * @Route("/deep/linker", name="deep_linker")
     */
    public function index(Request $request, ParameterBagInterface $parameterBag)
    {
        /**
         * @var DeepLinker $linker
         * &adj_t=t66ajrq_c8st647&adj_campaign=influencer&adj_adgroup=gizembarlak_24069
         * utm_source=gelirortaklari&utm_medium=influencer&utm_campaign=gizembarlak_24069
         */
        $json=null;
        switch ($request->request->get('medium'))
        {
            case 'boyner':
                $linker = new Boyner($parameterBag);
                break;
            case 'morhipo':
                $linker = new Morhipo($parameterBag);
                break;
        }
        if ($request->isMethod('post')) {
            $json = $linker->create(
                $request->request->get('comment'),
                $request->request->get('aff_sub'),
                $request->request->get('aff_id'),
                [
                    'adj_adgroup' => $request->request->get('adgroup')
                ],
                [
                    'utm_source' => 'gelirortaklari',
                    'utm_medium' => 'influencer',
                    'utm_campaign' => $request->request->get('adgroup')
                ]
            );

        }
        return $this->render('deep_linker/index.html.twig', [
            'medium'=>$request->request->get('medium'),
            'comment'=>$request->request->get('comment'),
            'aff_id'=>$request->request->get('aff_id'),
            'adgroup'=>$request->request->get('adgroup'),
            'aff_sub'=>$request->request->get('aff_sub'),
            'format'=>$json && property_exists($json,'url') ? $json->url->shortLink : 'Linki buradan alabilirsiniz.',
        ]);
    }
}
